# BlogIT

Blog reading made simple!

## Installation
Make sure you have latest __NodeJs LTS__ and __.NET core SdK >= 3.1.3__ installed.

## Launch app

### To peek at it

> dotnet run BlogIt.Api -c Release

Then go to _https://localhost:5001_ (if you experience trouble with SSL, see troubleshooting below).

Test user is: John / test123

### To develop

To launch back-end: 
> dotnet run BlogIt.Api

To launch front-end, go to _BlogIt.Api/client-app_ folder, then:
> yarn && yarn serve

OR
> npm i && npm run serve

(depending on your preference)

You can then open _https://localhost:8080_. Your browser will test you that page is not safe because autosigned.
You can safely go forward.

## More about development

### Project status
The project is complete from a functional point of view. Possible future enhancement: improve user management (register, logout).

The testing of the front part of the project is a work in progress.

### Testing

* Api is tested via Integration testing, tests are available in _BlogIt.Api.Tests_ project
* Postman tests can also be loaded from _postman_ folder
* You can launch front project unit tests from _BlogIt.Api/client-app_ folder, with:
> yarn test:unit

## Troubleshooting

If you encounter an SSL issue when launching the project, please check: https://docs.microsoft.com/en-us/aspnet/core/security/enforcing-ssl
