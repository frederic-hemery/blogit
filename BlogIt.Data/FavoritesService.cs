using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogIt.Context;
using BlogIt.Model;
using Microsoft.EntityFrameworkCore;

namespace BlogIt.Data
{
    public class FavoritesService
    {
        private ApplicationDbContext _context;
        
        public FavoritesService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task AddFavorite(Guid favId, Guid userId)
        {
            var post = await _context.Posts.FirstOrDefaultAsync(p => p.Id == favId);
            var user = (await _context.Users.Include(u => u.FavoritePosts).FirstOrDefaultAsync(u => u.Id == userId));
            user.FavoritePosts.Add(post);
            await _context.SaveChangesAsync();
        }

        public async Task<List<Post>> GetFavorites(Guid userId)
        {
            return await _context.Users
                .Where(u => u.Id == userId)
                .Include(u => u.FavoritePosts)
                .ThenInclude(p => p.Blogger)
                .Select(u => u.FavoritePosts).SingleOrDefaultAsync();
        }

        public async Task DeleteFavorite(Guid favId, Guid userId)
        {
            var user = await _context.Users.Include(u => u.FavoritePosts).FirstOrDefaultAsync(u => u.Id == userId);
            user.FavoritePosts.Remove(user.FavoritePosts.FirstOrDefault(p => p.Id == favId));
            await _context.SaveChangesAsync();
        }
    }
}