﻿using System.Linq;
using System.Threading.Tasks;
using BlogIt.Context;
using BlogIt.Model;
using IdentityModel;
using MyNovel.Dtos;

namespace BlogIt.Data
{
    public class UserService
    {
        private ApplicationDbContext _context;
        public UserService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<User> RegisterUser(UserRegistration registration)
        {
            if (!IsUserValid(registration))
            {
                return null;
            }
            var user = new User
            {
                Login = registration.Login,
                Password = registration.Password.ToSha256()
            };
            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();

            return new User() {Id= user.Id, Login = user.Login };
        }
        
        private bool IsUserValid(UserRegistration registration)
        {
            return IsUserNameAvailable(registration.Login) && IsPasswordValid(registration.Password);
        }

        private bool IsPasswordValid(string pwd)
        {
            if (string.IsNullOrEmpty(pwd))
            {
                return false;
            }
            var trimmedPwd = pwd.Trim();
            if (trimmedPwd.Length < 6)
            {
                return false;
            }

            return true;
        }

        private bool IsUserNameAvailable(string registrationLogin)
        {
            var existingUser = _context.Users.FirstOrDefault(u => u.Login == registrationLogin);
            return existingUser == null;
        }
    }
}