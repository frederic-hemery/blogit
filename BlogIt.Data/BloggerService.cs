using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlogIt.Context;
using BlogIt.Model;
using Microsoft.EntityFrameworkCore;

namespace BlogIt.Data
{
    public class BloggerService
    {
        private ApplicationDbContext _context;
        public BloggerService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<Blogger>> GetBloggers()
        {
            return await _context.Bloggers.ToListAsync();
        }

        public async Task<Blogger> GetById(Guid guid)
        {
            return await _context.Bloggers.Include(b => b.Posts).FirstOrDefaultAsync(b => b.Id == guid);
        }
    }
}