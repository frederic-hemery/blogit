using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using BlogIt.Context;
using BlogIt.Model;
using IdentityModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace BlogIt.Data
{
    public class LoginService
    {
        private ApplicationDbContext _context;
        public LoginService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ClaimsPrincipal> Login(UserLoginRequest loginRequest)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Login == loginRequest.Login);
            if (user == null || user.Password != loginRequest.Password.ToSha256())
            {
                return null;
            }

            return CreateClaimsPrincipal(user);
        }

        private ClaimsPrincipal CreateClaimsPrincipal(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.Login)
            };
            
            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            
            var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

            return claimsPrincipal;
        }
    }
}