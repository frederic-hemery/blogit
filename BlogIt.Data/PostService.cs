using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlogIt.Context;
using BlogIt.Model;
using Microsoft.EntityFrameworkCore;

namespace BlogIt.Data
{
    public class PostService
    {
        private ApplicationDbContext _context;
        public PostService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Post> GetById(Guid guid)
        {
            return await _context.Posts
                .Include(p => p.Blogger)
                .FirstOrDefaultAsync(b => b.Id == guid);
        }
    }
}