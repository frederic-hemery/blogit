using System;
using System.Collections.Generic;

namespace BlogIt.Model
{
    public class User
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        
        
        public List<Post> FavoritePosts { get; set; }
    }
}