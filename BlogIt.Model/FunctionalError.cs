﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogIt.Model
{
    public class FunctionalError
    {
        public List<String> Errors { get; set; }

        public FunctionalError()
        {
            Errors = new List<string>();
        }
    }
}
