﻿using System;

namespace MyNovel.Dtos
{
    public class UserRegistration
    {
        public string Login { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
