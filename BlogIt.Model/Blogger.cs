﻿using System;
using System.Collections.Generic;

namespace BlogIt.Model
{
    public class Blogger
    {
        public Guid Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Pitch { get; set; }
        public string AvatarUrl { get; set; }

        public List<Post> Posts { get; set; }
    }
}