using System;

namespace BlogIt.Model
{
    public class Post
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public Blogger Blogger { get; set; }
    }
}