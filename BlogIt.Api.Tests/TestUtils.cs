using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BlogIt.Api.TestData;
using Newtonsoft.Json;

namespace BlogIt.Api.Tests
{
    public static class TestUtils
    {
        public static async Task<HttpResponseMessage> LogUserIn(HttpClient client)
        {
            return await SendPostRequest(client, "api/login",
                new {Login = TestConstants.TestUserLogin, Password = TestConstants.TestUserPassword});
        }

        public static async Task<HttpResponseMessage> SendPostRequest(HttpClient client, string url, object payload)
        {
            var payloadAsJson =
                JsonConvert.SerializeObject(payload);
            HttpContent httpContent = new StringContent(payloadAsJson, Encoding.UTF8, "application/json");

            return await client.PostAsync(url, httpContent);
        }
        
        public static async Task<HttpResponseMessage> SendPutRequest(HttpClient client, string url, object payload)
        {
            var payloadAsJson =
                JsonConvert.SerializeObject(payload);
            HttpContent httpContent = new StringContent(payloadAsJson, Encoding.UTF8, "application/json");

            return await client.PutAsync(url, httpContent);
        }
    }
}