using System;
using System.Net.Http;
using BlogIt.Api.TestData;
using BlogIt.Context;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

[assembly: CollectionBehavior(DisableTestParallelization = true)]

namespace BlogIt.Api.Tests
{
    /**
     * The BaseApiTest creates an applicative context that can then be targeted by an HttpClient.
     *
     * With an in memory database set up, this enables easy integration testing.
     * More info: https://docs.microsoft.com/en-us/aspnet/core/test/integration-tests?view=aspnetcore-3.1
     */
    public class BaseApiTest : IClassFixture<WebApplicationFactory<Startup>>, IDisposable
    {
        private readonly WebApplicationFactory<Startup> _factory;

        protected HttpClient Client { get; private set; }

        public BaseApiTest(WebApplicationFactory<Startup> factory)
        {
            _factory = factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    var serviceProvider = services.BuildServiceProvider();

                    using (var scope = serviceProvider.CreateScope())
                    {
                        var scopedServices = scope.ServiceProvider;
                        var db = scopedServices
                            .GetRequiredService<ApplicationDbContext>();

                        try
                        {
                            TestDbPopulate.SetupData(db);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"An error occurred seeding " +
                                              $"the database with test messages. Error: {ex.Message}"
                            );
                        }
                    }
                });
            });
            Client = _factory.CreateClient(new WebApplicationFactoryClientOptions
            {
                HandleCookies = true
            });
        }

        public void Dispose()
        {
            Client?.Dispose();
            _factory?.Dispose();
        }
    }
}