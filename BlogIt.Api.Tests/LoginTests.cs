using System.Net;
using BlogIt.Api.TestData;
using Microsoft.AspNetCore.Mvc.Testing;
using Shouldly;
using Xunit;

namespace BlogIt.Api.Tests
{
    public class LoginTests : BaseApiTest
    {
        public LoginTests(WebApplicationFactory<Startup> factory) : base(factory)
        {
        }

        [Fact]
        public async void Login_Should_Return400_When_LoginRequestIsIncorrect()
        {
            // Act
            var response = await TestUtils.SendPostRequest(Client, "api/login", new {Login = "tom" });

            // Assert
            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async void Login_Should_Return201_When_LoginIsCorrect()
        {
            // Act
            var response = await TestUtils.SendPostRequest(Client, "api/login", new {Login = TestConstants.TestUserLogin, Password = TestConstants.TestUserPassword });
            
            // Assert
            response.StatusCode.ShouldBe(HttpStatusCode.OK);
        }

        [Theory]
        [InlineData("BadUser", "test123")]
        [InlineData("John", "badPassword")]
        public async void Login_Should_Return401_When_UserPasswordCombinationIsIncorrect(string login, string pwd)
        {
            // Act
            var response = await TestUtils.SendPostRequest(Client, "api/login", new {Login = login, Password = pwd });
            
            // Assert
            response.StatusCode.ShouldBe(HttpStatusCode.Unauthorized);
        }
        
        [Fact]
        public async void Login_Should_EnableUserToAccessProtectedPage()
        {
            await TestUtils.LogUserIn(Client);
            var response = await Client.GetAsync("api/favorites");
            
            // Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async void Logout_Should_Return200AndKillAuthentication()
        {
            await TestUtils.LogUserIn(Client);
            
            // Logout
            var logoutResponse = await Client.GetAsync("api/logout");
            logoutResponse.StatusCode.ShouldBe(HttpStatusCode.OK);
            
            // Check access
            var favoriteResponse = await Client.GetAsync("api/favorites");
            favoriteResponse.StatusCode.ShouldBe(HttpStatusCode.Unauthorized);
        }
    }
}