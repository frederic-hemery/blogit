using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using BlogIt.Api.TestData;
using BlogIt.Model;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using Shouldly;
using Xunit;

namespace BlogIt.Api.Tests
{
    public class FavoritesTests : BaseApiTest
    {
        public FavoritesTests(WebApplicationFactory<Startup> factory) : base(factory)
        {
        }
        
        [Fact]
        public async void Get_Favorites_Should_Return404_When_Unlogged()
        {
            // Act
            var response = await Client.GetAsync("api/favorites");

            // Assert
            response.StatusCode.ShouldBe(HttpStatusCode.Unauthorized);
        }
        
        [Fact]
        public async void Get_Favorites_Should_ReturnFavorites_When_LoggedIn()
        {
            // Act
            await TestUtils.LogUserIn(Client);   
            var response = await Client.GetAsync("api/favorites");

            // Assert
            response.EnsureSuccessStatusCode();
            var content = JsonConvert.DeserializeObject<List<Post>>(await response.Content.ReadAsStringAsync());
            
            content.Count().ShouldBe(1);
            content.First().Id.ShouldBe(TestConstants.JeanPaulSartreFirstPostGuid);
        }
        
        [Fact]
        public async void GettFavorites_Should_ReturnBloggerWithinPost()
        {
            // Arrange
            await TestUtils.LogUserIn(Client);

            // Act
            var response = await Client.GetAsync("api/favorites"); 

            // Assert
            var content = JsonConvert.DeserializeObject<List<Post>>(await response.Content.ReadAsStringAsync());
            content.First().Blogger.Id.ToString().ShouldBe(TestConstants.JeanPaulSartreGuid.ToString());

        }

        [Fact]
        public async void PutFavorites_Should_AddFavoriteToList()
        {
            // Act
            await TestUtils.LogUserIn(Client);
            var query = $"api/favorites/{TestConstants.JeanPaulSartreSecondPostGuid.ToString()}";
            var putResponse =
                await TestUtils.SendPutRequest(Client, query, null);
            putResponse.EnsureSuccessStatusCode();
            var response = await Client.GetAsync("api/favorites"); 

            // Assert
            var content = JsonConvert.DeserializeObject<List<Post>>(await response.Content.ReadAsStringAsync());
            content.Count().ShouldBe(2);
            content.Last().Id.ToString().ShouldBe(TestConstants.JeanPaulSartreSecondPostGuid.ToString());

        }
        
        [Fact]
        public async void DeleteFavorites_Should_AddFavoriteToList()
        {
            // Arrange
            await TestUtils.LogUserIn(Client);

            // Act
            var deleteResponse = await Client.DeleteAsync($"api/favorites/{TestConstants.JeanPaulSartreFirstPostGuid.ToString()}");
            deleteResponse.EnsureSuccessStatusCode();
            var response = await Client.GetAsync("api/favorites"); 

            // Assert
            var content = JsonConvert.DeserializeObject<List<Post>>(await response.Content.ReadAsStringAsync());
            content.Count().ShouldBe(0);

        }
    }
}