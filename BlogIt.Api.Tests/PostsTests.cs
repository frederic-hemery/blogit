using System.Collections.Generic;
using System.Linq;
using BlogIt.Api.TestData;
using BlogIt.Model;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using Shouldly;
using Xunit;

namespace BlogIt.Api.Tests
{
    public class PostsTests : BaseApiTest
    {
        public PostsTests(WebApplicationFactory<Startup> factory) : base(factory)
        {
        }
        
        [Fact]
        public async void Get_PostById_Should_ReturnPostAlongWithBlogger(){
            // Act
            var response = await Client.GetAsync($"api/posts/{TestConstants.JeanPaulSartreFirstPostGuid}");

            // Assert
            response.EnsureSuccessStatusCode(); // Status Code 200-299
            var result = JsonConvert.DeserializeObject<Post>(await response.Content.ReadAsStringAsync());
            result.ShouldNotBeNull();
            result.Id.ShouldBe(TestConstants.JeanPaulSartreFirstPostGuid);
            result.Blogger.Id.ShouldBe(TestConstants.JeanPaulSartreGuid);
        }
    }
}