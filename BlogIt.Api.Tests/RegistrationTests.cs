using System.Net;
using BlogIt.Api.TestData;
using Microsoft.AspNetCore.Mvc.Testing;
using Shouldly;
using Xunit;

namespace BlogIt.Api.Tests
{
    public class RegistrationTests : BaseApiTest
    {
        public RegistrationTests(WebApplicationFactory<Startup> factory) : base(factory)
        {
        }

        [Fact]
        public async void Register_ShouldBeSuccessFull()
        {
            // Act
            var response = await TestUtils.SendPostRequest(Client, "api/user", new {Login = "Tom", Password = "test123"});
            
            // Assert
            response.StatusCode.ShouldBe(HttpStatusCode.Created);
        }

        [Fact]
        public async void Register_ShouldReturn401_IfNameAlreadyExists()
        {
            // Act
            var response = await TestUtils.SendPostRequest(Client, "api/user", new {Login = TestConstants.TestUserLogin, Password = "test123"});
            
            // Assert
            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("abcde")]
        [InlineData("  abcde  ")]
        public async void Register_ShouldReturn400_IfPasswordIsTooShort(string pwd)
        {
            // Act
            var response = await TestUtils.SendPostRequest(Client, "api/user", new {Login = "Bob", Password = pwd});

            // Assert
            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
        }
    }
}