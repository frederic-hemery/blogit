using System.Collections.Generic;
using System.Linq;
using BlogIt.Api.TestData;
using BlogIt.Model;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using Shouldly;
using Xunit;

namespace BlogIt.Api.Tests
{
    public class BloggersTests : BaseApiTest
    {
        public BloggersTests(WebApplicationFactory<Startup> factory) : base(factory)
        {
        }
        
        [Fact]
        public async void Get_Bloggers_Should_Return_All_Bloggers()
        {
            // Act
            var response = await Client.GetAsync("api/bloggers");

            // Assert
            response.EnsureSuccessStatusCode(); // Status Code 200-299
            var result = JsonConvert.DeserializeObject<List<Blogger>>(await response.Content.ReadAsStringAsync());
            result.Count.ShouldBe(2);
            result.SingleOrDefault(b => b.FirstName == "Arthur").ShouldNotBeNull();
        }
        
        [Fact]
        public async void Get_Bloggers_Should_NotReturnPosts()
        {
            // Act
            var response = await Client.GetAsync("api/bloggers");

            // Assert
            var result = JsonConvert.DeserializeObject<List<Blogger>>(await response.Content.ReadAsStringAsync());
            var arthur = result.Single(b => b.FirstName == "Arthur");
            arthur.Posts.ShouldBeNull();
        }

        [Fact]
        public async void GetBloggerById_Should_ReturnBloggerPosts()
        {
            // Act
            var response = await Client.GetAsync($"api/bloggers/{TestConstants.JeanPaulSartreGuid.ToString()}");

            response.EnsureSuccessStatusCode();
            var result = JsonConvert.DeserializeObject<Blogger>(await response.Content.ReadAsStringAsync());
            result.Posts.Count().ShouldBe(2);
        }
    }
}