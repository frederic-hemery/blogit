import {
  shallowMount,
} from '@vue/test-utils';
import NavigationBar from '@/components/shared/NavigationBar.vue';
import localVue from '../../base';

describe('NavigationBar.vue', () => {
  beforeEach(() => {
  });

  it('displays properly', () => {
    const wrapper = shallowMount(NavigationBar, {
      localVue,
    });
    expect(wrapper.text()).toMatch('BlogIt');
  });
});
