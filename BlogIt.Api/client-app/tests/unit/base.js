import Vue from 'vue';
import Vuetify from 'vuetify';
import Vuex from 'vuex';
import Router from 'vue-router';
import { createLocalVue } from '@vue/test-utils';

Vue.use(Vuetify);
Vue.use(Router);
Vue.use(Vuex);

const localVue = createLocalVue();
export default localVue;
