import userStore from '../../../../../src/store/modules/user';

describe('user store', () => {
  describe('mutations', () => {
    describe('setFavorites', () => {
      it('should add the post, adding them as favorite', () => {
        const state = { favorites: [] };
        userStore.mutations.setFavorites(state, [{ id: 12 }]);

        expect(state.favorites.length).toBe(1);
        expect(state.favorites[0].id).toBe(12);
        expect(state.favorites[0].isFavorite).toBe(true);
      });
    });

    describe('setLoggedIn', () => {
      it('should set whether player is loggedIn', () => {
        const state = { isLoggedIn: false };
        userStore.mutations.setLoggedIn(state, true);

        expect(state.isLoggedIn).toBe(true);
      });
    });

    describe('setLoginAttemptFailed', () => {
      it('should set whether login attempt failed', () => {
        const state = { didLoginAttemptFail: false };
        userStore.mutations.setLoginAttemptFailed(state, true);

        expect(state.didLoginAttemptFail).toBe(true);
      });
    });
  });
});
