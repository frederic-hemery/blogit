import blogStore from '../../../../../src/store/modules/blog';

describe('blog Store', () => {
  describe('mutations', () => {
    describe('setCurrentPost', () => {
      it('should set the current post, and set it as favorite if it is', () => {
        const state = { favorites: [{ id: 12 }], currentPost: null };
        const post = { id: 12, title: 'post' };
        blogStore.mutations.setCurrentPost(state, post);

        expect(state.currentPost).toEqual({ ...post, isFavorite: true });
      });

      it('should not flag as favorite if not', () => {
        const state = { favorites: [{ id: 12 }], currentPost: null };
        const post = { id: 42 };
        blogStore.mutations.setCurrentPost(state, post);

        expect(state.currentPost).toEqual({ ...post, isFavorite: false });
      });
    });

    describe('setCurrentBlogger', () => {
      it('should just set the blogger', () => {
        const state = { currentBlogger: true };
        const currentBlogger = { id: 23 };
        blogStore.mutations.setCurrentBlogger(state, currentBlogger);
        expect(state.currentBlogger).toEqual(currentBlogger);
      });
    });

    describe('setBloggers', () => {
      it('should just set the bloggers', () => {
        const state = { bloggers: null };
        const bloggers = [{ id: 123 }];
        blogStore.mutations.setBloggers(state, bloggers);

        expect(state.bloggers).toEqual(bloggers);
      });
    });
  });

  describe('StoreblogStore.getters', () => {
    describe('blogger', () => {
      it('should return null if no blogger', () => {
        expect(blogStore.getters.blogger({ currentBlogger: null })).toBeNull();
      });

      it('should return the blogger if no favorite is configured', () => {
        const state = { currentBlogger: { id: 23, posts: [{ id: 42 }] } };
        const blogger = blogStore.getters.blogger(state, null, { user: { favorite: [] } });
        expect(blogger).toEqual({ ...blogger, posts: [{ id: 42, isFavorite: false }] });
      });

      it('should return whether blogger posts are favorites', () => {
        const state = {
          currentBlogger: { id: 23, posts: [{ id: 42 }, { id: 43 }] },

        };
        const rootState = {
          user: {
            favorites: [{ id: 42 }],
          },
        };

        const blogger = blogStore.getters.blogger(state, null, rootState);

        expect(blogger.posts.find((p) => p.id === 42).isFavorite).toBe(true);
        expect(blogger.posts.find((p) => p.id === 43).isFavorite).toBe(false);
      });
    });

    describe('post', () => {
      it('should return currentPost', () => {
        const state = { currentPost: null };

        expect(blogStore.getters.post(state, null, null)).toBeNull();
      });

      it('should return not favorite when no favorite is defined', () => {
        const post = { id: 1 };
        const state = { currentPost: post };
        const rootState = {
          user: {
            favorites: [],
          },
        };

        expect(blogStore.getters.post(state, null, rootState))
          .toEqual({ ...post, isFavorite: false });
      });

      it('should return not favorite when current post is not a user\'s favorite', () => {
        const post = { id: 1 };
        const state = { currentPost: post };
        const rootState = {
          user: {
            favorites: [{ id: 2 }],
          },
        };

        expect(blogStore.getters.post(state, null, rootState))
          .toEqual({ ...post, isFavorite: false });
      });

      it('should return favorite when current post IS a user\'s favorite', () => {
        const post = { id: 1 };
        const state = { currentPost: post };

        const rootState = {
          user: {
            favorites: [{ id: 1 }],
          },
        };

        expect(blogStore.getters.post(state, null, rootState))
          .toEqual({ ...post, isFavorite: true });
      });
    });
  });
});
