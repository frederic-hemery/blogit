import {
  shallowMount,
} from '@vue/test-utils';
import Vuex from 'vuex';
import localVue from '../base';
import BlogPostView from '../../../src/views/BlogPostView.vue';


describe('BlogPostView.vue', () => {
  let wrapper;
  let getters;
  let store;
  let state;

  beforeEach(() => {
    state = { post: null };
    getters = {
      'blog/post': () => state.post,
    };
    store = new Vuex.Store({
      getters,
      state,
    });
    store.dispatch = jest.fn();

    wrapper = shallowMount(BlogPostView, {
      stubs: { BlogPost: true },
      propsData: { id: 42 },
      localVue,
      store,
    });
  });

  describe('initially', () => {
    it('should display nothing', () => {
      expect(wrapper.html()).not.toContain('blogpost-stub');
    });

    it('should dispatch retrieval of current post', () => {
      expect(store.dispatch).toHaveBeenCalledWith('blog/fetchCurrentPost', 42);
    });
  });

  describe('when post data arrives', () => {
    beforeEach(async () => {
      state.post = {};
      await wrapper.vm.$forceUpdate();
    });
    it('should display the blog post', async () => {
      expect(wrapper.html()).toContain('blogpost-stub');
    });
  });
});
