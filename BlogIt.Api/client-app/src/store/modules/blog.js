import postRepository from '../repositories/post-repository';
import bloggerRepository from '../repositories/blogger-repositoy';

export default {
  namespaced: true,
  state: {
    currentPost: null,
    currentBlogger: null,
    bloggers: null,
  },
  mutations: {
    setCurrentPost(state, post) {
      if (!post) {
        this.state.currentPost = null;
        return;
      }
      const isFavorite = post && !!state.favorites?.find((f) => f.id === post.id);
      state.currentPost = { ...post, isFavorite };
    },
    setCurrentBlogger(state, blogger) {
      state.currentBlogger = blogger;
    },
    setBloggers(state, bloggers) {
      state.bloggers = bloggers;
    },
  },
  getters: {
    blogger(state, getters, rootState) {
      const blogger = state.currentBlogger;
      if (!blogger) {
        return null;
      }
      const favorites = rootState.user.favorites || [];

      return {
        ...blogger,
        posts: blogger.posts.map((p) => ({
          ...p,
          isFavorite: !!favorites.find((f) => f.id === p.id),
        })),
      };
    },
    post(state, getters, rootState) {
      const { currentPost } = state;
      if (!currentPost) {
        return null;
      }
      const { favorites } = rootState.user;
      const isFavorite = !!favorites
        && !!favorites?.find((f) => f.id === currentPost.id);
      return { ...currentPost, isFavorite };
    },
  },
  actions: {
    fetchCurrentPost({ commit }, id) {
      postRepository.getPostById(id).then(({ data }) => {
        commit('setCurrentPost', data);
      });
    },
    resetCurrentPost({ commit }) {
      commit('setCurrentPost', null);
    },
    getBloggers({ commit }) {
      bloggerRepository.getBloggers().then(({ data }) => {
        commit('setBloggers', data);
      });
    },
    fetchBlogger({ commit }, id) {
      bloggerRepository.getBloggerById(id).then(({ data }) => {
        commit('setCurrentBlogger', data);
      });
    },
    resetBlogger({ commit }) {
      commit('setCurrentBlogger', null);
    },
  },
};
