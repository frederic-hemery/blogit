import favoritesRepository from '../repositories/favorites-repository';
import loginRepository from '../repositories/login-repository';

export default {
  namespaced: true,
  state: {
    favorites: null,
    isLoggedIn: false,
    didLoginAttemptFail: false,
  },
  mutations: {
    setFavorites(state, favorites) {
      state.favorites = favorites.map((f) => ({ ...f, isFavorite: true }));
    },
    setLoggedIn(state, isLoggedIn) {
      state.isLoggedIn = isLoggedIn;
    },
    setLoginAttemptFailed(state, isFailedAttempt) {
      state.didLoginAttemptFail = isFailedAttempt;
    },
  },
  actions: {
    getFavorites({ commit }) {
      favoritesRepository.getFavorites().then(({ data }) => {
        commit('setFavorites', data);
        commit('setLoggedIn', true);
      }).catch(() => {
        commit('setLoggedIn', false);
      });
    },
    addFavorite({ commit, state }, favorite) {
      favoritesRepository.addFavorite(favorite.id).then(() => {
        commit('setFavorites', state.favorites.concat([favorite]));
      });
    },
    deleteFavorite({ commit, state }, favorite) {
      favoritesRepository.deleteFavorite(favorite.id).then(() => {
        commit('setFavorites', state.favorites.filter((f) => f.id !== favorite.id));
      });
    },
    login({ commit, dispatch }, { login, password }) {
      loginRepository.login(login, password).then(() => {
        commit('setLoggedIn', true);
        commit('setLoginAttemptFailed', false);
        dispatch('getFavorites');
      }).catch(() => {
        commit('setLoggedIn', false);
        commit('setLoginAttemptFailed', true);
      });
    },
  },
};
