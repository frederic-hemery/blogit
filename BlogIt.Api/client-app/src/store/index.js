import Vue from 'vue';
import Vuex from 'vuex';
import userModule from './modules/user';
import blogModule from './modules/blog';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    user: userModule,
    blog: blogModule,
  },
});
