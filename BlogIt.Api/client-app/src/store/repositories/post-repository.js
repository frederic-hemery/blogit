import axios from 'axios';

export default {
  getPostById: (id) => axios.get(`/api/posts/${id}`),
};
