import axios from 'axios';

export default {
  getFavorites: () => axios.get('/api/favorites'),
  addFavorite: (favId) => axios.put(`/api/favorites/${favId}`, {}),
  deleteFavorite: (favId) => axios.delete(`/api/favorites/${favId}`),
};
