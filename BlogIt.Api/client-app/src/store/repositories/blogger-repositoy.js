import axios from 'axios';

export default {
  getBloggerById: (id) => axios.get(`/api/bloggers/${id}`),
  getBloggers: () => axios.get('/api/bloggers'),
};
