import axios from 'axios';

export default {
  login: (login, password) => axios.post('/api/login', { login, password }),
};
