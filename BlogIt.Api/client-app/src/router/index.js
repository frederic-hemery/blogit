import Vue from 'vue';
import Router from 'vue-router';
import HomePage from '../views/HomeView.vue';
import BloggerPage from '../views/BloggerView.vue';
import PostPage from '../views/BlogPostView.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/blogger/:id',
      name: 'Blogger',
      component: BloggerPage,
      props: true,
    }, {
      path: '/post/:id',
      name: 'Post',
      component: PostPage,
      props: true,
    }, {
      path: '/',
      name: 'Home',
      component: HomePage,
    }],
});
