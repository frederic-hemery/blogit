module.exports = {
  devServer: {
    https: true,
    proxy: {
      '/api': {
        target: 'https://localhost:5001',
        changeOrigin: true,
        secure: false,
      },
    },
  },
  transpileDependencies: [
    'vuetify',
  ],
};
