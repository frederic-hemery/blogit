using System.Collections.Generic;
using System.Linq;
using BlogIt.Context;
using BlogIt.Model;
using IdentityModel;

namespace BlogIt.Api.TestData
{
    public static class TestDbPopulate
    {
        public static void SetupData(ApplicationDbContext context)
        {
            DeleteExisting(context);
            PopulateDb(context);
            context.SaveChanges();
        }

        private static void PopulateDb(ApplicationDbContext context)
        {
            var sartre = GetJeanPauSartre();
            context.Bloggers.Add(sartre);
            context.Bloggers.Add(GetArthur());

            context.Users.Add(new User()
            {
                Id = TestConstants.TestUserGuid,
                Login = TestConstants.TestUserLogin,
                Password = TestConstants.TestUserPassword.ToSha256(),
                FavoritePosts = new List<Post> {sartre.Posts.First()}
            });
        }

        private static void DeleteExisting(ApplicationDbContext context)
        {
            context.Users.RemoveRange(context.Users.ToList());
            context.Bloggers.RemoveRange(context.Bloggers.ToList());
            context.Posts.RemoveRange(context.Posts.ToList());
        }

        private static Blogger GetJeanPauSartre()
        {
            return new Blogger
            {
                Id = TestConstants.JeanPaulSartreGuid,
                FirstName = "Jean-Paul",
                LastName = "Sartre",
                Pitch = "Sur cet écritoire se distillent mes pensées les plus profondes et les plus lumineuses",
                AvatarUrl =
                    "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Jean_Paul_Sartre_1965.jpg/260px-Jean_Paul_Sartre_1965.jpg",
                Posts = new List<Post>
                {
                    new Post
                    {
                        Id = TestConstants.JeanPaulSartreFirstPostGuid,
                        Title = "L'épée émoussée",
                        Content = "Longtemps j’ai pris ma plume pour une épée: à présent, " +
                                  "je connais notre impuissance. N’importe: je fais, je ferais des livres; " +
                                  "il en faut; cela sert tout de même. La culture ne sauve rien ni personne, " +
                                  "elle ne justifie pas. Mais c’est un produit de l’homme. " +
                                  "Il s’y projette, s’y reconnaît; seul, ce miroir critique lui offre son image. "
                    },
                    new Post
                    {
                        Id = TestConstants.JeanPaulSartreSecondPostGuid,
                        Title = "La raison est-elle ce que la raison pense être ?",
                        Content =
                            "Je commençais à me découvrir. Je n'étais presque rien, tout au plus une activité sans" +
                            " contenu, mais il n'en fallait pas davantage. J'échappais à la comédie : je ne " +
                            "travaillais pas encore mais déjà je ne jouais plus, le menteur trouvait sa vérité " +
                            "dans l'élaboration des ses mensonges. Je suis né de l'écriture : avant elle, il n'y " +
                            "avait qu'un jeu de miroirs ; dès mon premier roman, je sus qu'un enfant s'était " +
                            "introduit dans le palais de glaces. Ecrivant, j'existais, j'échappais aux grandes " +
                            "personnes ; mais je n'existais que pour écrire et si je disais : moi, cela" +
                            " signifiât, moi qui écris."
                    }
                }
            };
        }

        private static Blogger GetArthur()
        {
            return new Blogger
            {
                FirstName = "Arthur",
                LastName = "Roi de Bretagne",
                Pitch =
                    "Régner sur la Bretagne, c'est pas facile tous les jours. Partagez donc quelques moments de mon quotidien",
                AvatarUrl =
                    "https://vignette.wikia.nocookie.net/kaamelott-france/images/4/40/Alexandre-ASTIER_portrait_w858-1-.jpg/revision/latest?cb=20130420223901&path-prefix=fr",
                Posts = new List<Post>
                {
                    new Post
                    {
                        Title = "La table ronde",
                        Content = "ARTHUR, LÉODAGAN et PERCEVAL s’apprêtent à entrer dans la grande salle.<br/><br/>"
                                  + "LÉODAGAN (à Arthur) — Je vois pas ce que c’est que cette lubie de vous faire fabriquer une table.<br/><br/>"
                                  + " PERCEVAL — D’autant qu’il y en a une de douze pieds dans la salle à manger…<br/><br/>"
                                  + "ARTHUR (épique) — Celle-là, elle est ronde. C’est une table autour de laquelle les Chevaliers de Bretagne se retrouveront pour unir leurs forces et leurs destinées. D’ailleurs, autant vous y faire parce qu’à partir de maintenant, on va tous s’appeler « les Chevaliers de la Table Ronde. »<br/><br/>"
                                  + "PERCEVAL — « Les Chevaliers de la Table Ronde » ?"
                                  + "LÉODAGAN — Une chance que vous vous soyez pas fait construire un buffet à vaisselle… "
                    },
                    new Post
                    {
                        Title = "La salle du trône",
                        Content =
                            "ARTHUR et LÉODAGAN s’apprêtent à entrer dans la salle du Trône où ils sont attendus. BOHORT, apeuré, les retient.<br><br>"
                            + "BOHORT — Sire, je vous en conjure, n’entrez pas dans cette pièce !<br><br>"
                            + "ARTHUR — Pourquoi donc ? C’est la salle du Trône… Ce serait quand même gros que je puisse pas y entrer !<br><br>"
                            + "BOHORT — Mais Attila vous y attend, Sire ! Attila ! Le Fléau de Dieu !<br><br>"
                            + "ARTHUR (à Léodagan) — C’est sûr que c’est pas Jo le Rigolo…<br><br>"
                            + "BOHORT — Jo le Rigolo ? On dit que là où il passe, l’herbe ne repousse pas !<br><br>"
                            + "LÉODAGAN — Il y a pas d’herbe dans la salle du Trône."
                    },
                    new Post
                    {
                        Title = "Les réunions passionantes",
                        Content = "ARTHUR (à Père Blaise) — Bon bah voilà. Vous vous en êtes sorti ?<br><br>"
                                  + "PÈRE BLAISE — Oui, oui. C’est magnifique. Il y a des taches, c’est tout barré, j’ai tellement raturé que j’ai transpercé le papier, c’est immonde, on dirait que j’ai lavé par terre avec mais c’est fait.<br><br>"
                                  + "LANCELOT — Ce qui compte, c’est ce que ça raconte, non ?<br><br>"
                                  + "ARTHUR — Sans vouloir être blessant, ce que ça raconte, ça casse pas des briques non plus.<br><br>"
                                  + "BOHORT (à Père Blaise) — Vous aurez peut-être le temps de le remettre au propre plus tard ?<br><br>"
                                  + "CALOGRENANT — Ah oui parce que ce serait dommage de perdre ça !<br><br>"
                                  + "LÉODAGAN — La Fabuleuse Légende Du Cheval Malade De Perceval. "
                    }
                }
            };
        }
    }
}