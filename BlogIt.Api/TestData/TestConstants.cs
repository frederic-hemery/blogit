using System;

namespace BlogIt.Api.TestData
{
    public static class TestConstants
    {
        public static string TestUserLogin = "John";
        public static string TestUserPassword = "test123";
        public static Guid TestUserGuid = new Guid("ef910d3d-14b9-44fd-b0cc-44e8728e83d5");
        public static Guid JeanPaulSartreGuid = new Guid("a9fe7131-443e-4507-ace5-228f6e9a74e7");
        public static Guid JeanPaulSartreFirstPostGuid = new Guid("0a7a724c-f4b9-4e70-8ea0-230039f6305d");
        public static Guid JeanPaulSartreSecondPostGuid = new Guid("e3e33f6b-afae-47c5-90e9-89a4af4c7449");
    }
}