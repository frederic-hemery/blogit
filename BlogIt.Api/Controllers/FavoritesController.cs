using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using BlogIt.Model;
using BlogIt.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BlogIt.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/favorites")]
    public class FavoritesController : ControllerBase
    {
        private FavoritesService _favoritesService;

        public FavoritesController(FavoritesService favoritesService)
        {
            _favoritesService = favoritesService;
        }
        
        // GET
        [HttpGet]
        public async Task<List<Post>> ListFavorites()
        {
            var userId = HttpContext.User.FindFirst(c => c.Type == ClaimTypes.NameIdentifier);
            return await _favoritesService.GetFavorites(new Guid(userId.Value));
        }

        [HttpPut]
        [Route("{postId}")]
        public async Task<IActionResult> AddFavorite(Guid postId)
        {
            var userId = HttpContext.User.FindFirst(c => c.Type == ClaimTypes.NameIdentifier);
            await _favoritesService.AddFavorite(postId, new Guid(userId.Value));
            return Ok();
        }

        [HttpDelete]
        [Route("{postId}")]
        public async Task<IActionResult> DeleteFavorite(Guid postId)
        {
            var userId = HttpContext.User.FindFirst(c => c.Type == ClaimTypes.NameIdentifier);
            await _favoritesService.DeleteFavorite(postId, new Guid(userId.Value));
            return Ok();
        }
    }
}