using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BlogIt.Context;
using BlogIt.Model;
using BlogIt.Data;
using Microsoft.AspNetCore.Mvc;

namespace BlogIt.Controllers
{
    [ApiController]
    [Route("api/bloggers")]
    public class BloggerController : ControllerBase
    {
        private BloggerService _bloggerService;

        public BloggerController(BloggerService bloggerService)
        {
            _bloggerService = bloggerService;
        }

        [HttpGet]
        [Route("")]
        public async Task<List<Blogger>> GetBloggers()
        {
            return await _bloggerService.GetBloggers();
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<Blogger> GetBloggerById(Guid id)
        {
            var blogger = await _bloggerService.GetById(id);
            return blogger;
        }
    }
}