using System;
using System.Threading.Tasks;
using BlogIt.Model;
using BlogIt.Data;
using Microsoft.AspNetCore.Mvc;

namespace BlogIt.Controllers
{
    [ApiController]
    [Route("api/posts")]
    public class PostController : ControllerBase
    {
        private PostService _postService;

        public PostController(PostService postService)
        {
            _postService = postService;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<Post> GetPostById(Guid id)
        {
            return await _postService.GetById(id);
        }
    }
}