﻿using Microsoft.AspNetCore.Mvc;
using MyNovel.Dtos;
using System.Threading.Tasks;
using BlogIt.Data;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BlogIt.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private UserService _userSvc;

        public UserController(UserService userSvc)
        {
            _userSvc = userSvc;
        }

        [HttpPost]
        public async Task<ObjectResult> Register([FromBody] UserRegistration registration)
        {
            var user = await _userSvc.RegisterUser(registration);
            if (user == null)
            {
                return BadRequest("");
            }

            return Created("/api/user/" + user.Id.ToString(), user);
        }
    }
}