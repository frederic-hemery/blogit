using System.Threading.Tasks;
using BlogIt.Context;
using BlogIt.Model;
using BlogIt.Data;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;

namespace BlogIt.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api")]
    public class LoginController : Controller
    {
        private LoginService _loginService;

        public LoginController(LoginService loginService)
        {
            _loginService = loginService;
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] UserLoginRequest loginRequest)
        {
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(ModelState);
            }

            var claimsPrincipal = await _loginService.Login(loginRequest); 
            if (claimsPrincipal == null)
            {
                return Unauthorized("Invalid login/pwd combination");
            }

            await HttpContext.SignInAsync(claimsPrincipal);

            return Ok();
        }

        [HttpGet("logout")]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return Ok();
        }
    }
}